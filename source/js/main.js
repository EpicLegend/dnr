'use strict';
//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/jquery/dist/jquery.js
//= ../node_modules/popper.js/dist/umd/popper.js
//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
//= library/jquery-ui.js
//= library/slick.js


$(document).ready(function () {




	/* START Открытие меню */
	$(".btn__menu").on("click", function () {
		$(this).toggleClass("active");



		if ( $(this).hasClass("active") ) {
			$(".mobile-menu").addClass("show");
			$("body").css("overflow", "hidden");
		} else {
			$(".mobile-menu").removeClass("show");
			$("#burger").removeClass("active");
			$("body").css("overflow", "auto");
		}
	});
	$(".mobile-menu").on("click", function () {
		$(this).toggleClass("show");
		$(".btn__menu").removeClass("active");
		$("#burger").removeClass("active");
		$("body").css("overflow", "auto");
	});

	$(".btn-drop-menu").on("click", function(e) {
		var id = $(this).attr("href");
		$(id).collapse("toggle");
	});
	$(".mobile-menu-content").on("click", function (e) {
		e.stopPropagation();
		e.preventDefault();
	});

	$(".btn_header").on("click", function () {
		console.log(" 2 2  2btn__menu");
		if( $(".navigation__content").hasClass("active") ) {
			$(".navigation__content").removeClass("active");
			$(".btn__menu").removeClass("active");
			$("#burger").removeClass("active");
		}
	});
	/* END откртие меню*/


	if ( $(window).scrollTop() > 0 ) {
	}
	$(window).on("scroll", function () {

		scrollMenu();

		if ( $(window).scrollTop() > 0 ) {
		}  else {
		}

		/* START кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn-to-top").addClass("btn-to-top_active");
		} else {
			$(".btn-to-top").removeClass("btn-to-top_active");
		}

	});
	$('.btn-to-top').click(function () {
		$('body,html').animate({
			scrollTop: 0
			}, 400);
		return false;
	});
	/* END кнопка вверх */

	function scrollMenu() {
		var menuHeight = $(".header").height();

		if( $(window).scrollTop() > menuHeight ) {
			$("body").css("padding-top", menuHeight + "px");
			$(".header").addClass("header_scroll");
		} else {
			$("body").css("padding-top", 0 + "px");
			$(".header").removeClass("header_scroll");
		}

	}

	$(".welcome-slider").each(function(index, element) {
		$(element).slick({
			settings: "unslick",
			autoplay: true,
			autoplaySpeed: 3000,
			fade: true,
			cssEase: 'linear',
			speed: 300,
			infinite: true,
			dots: false,
			arrows: false,
			slidesToShow: 1,
			slidesToScroll: 1
		});
	});
	$(".partners-slider").each(function(index, element) {
		$(element).slick({
			settings: "unslick",
			speed: 300,
			infinite: true,
			centerPadding: '30px',
			dots: false,
			arrows: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			nextArrow: '<div class="arrow arrow_right"><i class="icon-arrow"></i></div>',
			prevArrow: '<div class="arrow arrow_left"><i class="icon-arrow"></i></div>',
			responsive: [
				{
					breakpoint: 768,
					settings: {
						arrows: false,
						centerPadding: '40px',
						slidesToShow: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						arrows: false,
						centerPadding: '40px',
						slidesToShow: 1
					}
				}
			]
		});
	});
	$(".content-slider").each(function(index, element) {
		$(element).slick({
			settings: "unslick",
			speed: 300,
			infinite: false,
			dots: false,
			arrows: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			nextArrow: '<div class="arrow arrow_right"><i class="icon-arrow"></i></div>',
			prevArrow: '<div class="arrow arrow_left"><i class="icon-arrow"></i></div>'
		});
	});


	//  КАЛЕНДАРЬ
	/* Локализация datepicker */
	$.datepicker.regional.ru = {
		closeText: "Закрыть",
		prevText: "&#x3C;Пред",
		nextText: "След&#x3E;",
		currentText: "Сегодня",
		monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь",
		"Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" ],
		monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн",
		"Июл","Авг","Сен","Окт","Ноя","Дек" ],
		dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота" ],
		dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт" ],
		dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб" ],
		weekHeader: "Нед",
		dateFormat: "dd.mm.yy",
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ""
	};
	$.datepicker.setDefaults( $.datepicker.regional.ru );

	var eventDays = [
		[14,10],
		[9,10],
		[4,10]
	];

	$("#datepicker").datepicker({
		onSelect: function(date){
			$('#datepicker_value').val(date)
		},
		beforeShowDay: function(date){
			for (var i = 0; i < eventDays.length; i++) {
				if (eventDays[i][0] == date.getDate() && eventDays[i][1] - 1 == date.getMonth()) {
					return [true, "event-now"];
				}
			}
			return [true];
		}
	});
	$("#datepicker").datepicker("setDate", $('#datepicker_value').val());


});
